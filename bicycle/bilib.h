#include <math.h>
#include <stdio.h>

double poly_kernel( double d,
                    double* state1, double* state2);

double gaussian_kernel( double sigma,
                        double* state1, double* state2);

void current( double* row,
              double* state, double param, double* s, int srows,
              double (*kernel)(double,double*,double*));

double next_state(double* state, double a, double* nextstate);

void normstate(double* state, double* normed);

void calcPolys(double* state, double* row);
