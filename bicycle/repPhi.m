function [newPhi] = repPhi(Phi, A, numAc)

[n k]=size(Phi);
newPhi=zeros(n,numAc*k);
for i=1:size(Phi,1)
  newPhi(i,k*(A(i)-1)+1:k*(A(i)))=Phi(i,:);
end
newPhi=kron(newPhi,ones(numAc,1));

%Phi = kron(Phi, ones(numAc, 1));
%newPhi = zeros(size(Phi));
%for i = 0:size(Phi, 1) / numAc - 1
%	newPhi(i*numAc+1:i*numAc+numAc, size(Phi, 2) * A(i + 1) + 1:size(Phi, 2) * A(i + 1) + size(Phi, 2)) = Phi(i*numAc+1:i*numAc+numAc, :);
%end
%clear Phi;
