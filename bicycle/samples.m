classdef samples < handle

%%%%%%%%%%%%%%%%%
% samples.m
%
% MATLAB class to handle samples for RL problems
%
% s:  Vector of samples of size (n,*)
% a:  Vector of actions of size (n,1)
% r:  Vector of rewards of size (n,1)
% sp: Vector of next states of size (n,*)
%%%%%%%%%%%%%%%%%

  properties
    s
    a
    r
    sp
  end

  methods
    function obj = samples(s, a, r, sp)
      obj.s=s;
      obj.a=a;
      obj.r=r;
      obj.sp=sp;
    end
  end
end
