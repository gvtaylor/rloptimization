#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <limits.h>
#include <signal.h>
#include <sys/times.h>
#include <sys/time.h>
#include <errno.h>


double sign(double x);
double max(double x, double y);
void bicycle(double *nextstate, double *reward, double *endsim, 
	     double *istate,  double *action, int to_do, double *maxnoise);
double calc_dist_to_goal(double xf, double xb, double yf, double yb);
double calc_angle_to_goal(double xf, double xb, double yf, double yb);
double orig_calc_angle_to_goal(double xf, double xb, double yf, double yb);
