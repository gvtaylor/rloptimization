function [Phi, wfactors, alpha] = standardize(X)
[n,k] = size(X);

alpha=mean(X);
Phi=(X - repmat(alpha,n,1));
wfactors = sum(abs(Phi))/(size(Phi,1)/4);
%wfactors(find(wfactors==0))=eps;
Phi=Phi ./ repmat(wfactors,n,1);
