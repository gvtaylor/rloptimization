function [w result time]=ralp(data,lastresult)

rho=data.rho;
gamma=data.gamma;
Phi=data.features{1};
Phip=data.features{2};
%Below for Q-functions
%R=kron(data.samples.r,ones(5,1));
R=data.samples.r;
psi=data.t;

%numActions=5; %hardcoded

n=size(Phi,1);
k=size(Phi,2);

model.A=sparse([
[Phi zeros(n,k)]-gamma*[Phip zeros(n,k)];
eye(k) eye(k);
-eye(k) eye(k);
%zeros(1,k) ones(1,k)
zeros(1,k) 0 ones(1,k-1)
]);
%for i=1:numActions
  %model.A(end,k+(i-1)*k/numActions+1)=0;
%end

model.sense=[repmat('>',1,n+2*k) '<'];
model.rhs=[R; zeros(2*k,1); psi];
model.obj=[rho*Phi zeros(1,k)];
model.lb=[-Inf(1,k) zeros(1,k)];

if nargin==2
  model.vbasis=lastresult.vbasis;
  model.cbasis=lastresult.cbasis;
end

param.OutputFlag=0;

tic;
result=gurobi(model,param);
time=toc;
w=0;
if strcmp(result.status, 'OPTIMAL')
  w=result.x(1:k);
else
  fprintf('Optimization returned status: %s\n', result.status);
end
