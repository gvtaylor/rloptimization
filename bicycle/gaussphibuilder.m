function Phi=gaussphibuilder(means,states,sigma)

n=size(states,1);
k=size(means,1);
normer=[1.3963 9.2930 .2094 1.7527 8.4872 3.1319];
normMeans=10*means(:,1:6)./repmat(normer,k,1);
normStates=10*states(:,1:6)./repmat(normer,n,1);

Phi=zeros(n,k);
Sigma=sigma*eye(6);
for i=1:k
  Phi(:,i)=mvnpdf(normStates,normMeans(i,:),Sigma);
end
