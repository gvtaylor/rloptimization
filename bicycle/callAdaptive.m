%function [w time] = callAdaptive(data)

rho=data.rho;
gamma=data.gamma;
Phi=data.features{1};
Phip=data.features{2};
R=data.samples.r;
psi=data.t;

n=size(Phi,1);
k=size(Phi,2);

f=[rho*Phi zeros(1,k)]';
Ain=[ gamma*Phip-Phi zeros(n,k);
      -eye(k) -eye(k);
      eye(k)  -eye(k);
      zeros(1,k) 0 ones(1,k-1)];
bin=[-R; zeros(2*k,1); psi ];
Aeq=[];
beq=[];
ub=Inf(2*k,1);
lb=[-Inf(k,1); zeros(k,1)];
tic;
[sol out]=pdhg_lp(f,Ain,bin,Aeq,beq,ub,lb);
time=toc;
w=sol(1:k);
