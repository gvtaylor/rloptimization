function [PhiP] = repPhiPrime(PhiP,actions)

%PhiP = phiBuilder(SP, n);
[rows cols]= size(PhiP);
newPhiP = zeros(rows*actions,cols*actions);
features = size(PhiP,2);
PhiP = kron(PhiP,ones(actions,1));
count=0;
for i=1:rows*actions
  newPhiP(i,cols*count+1:cols*(count+1))=PhiP(i,:);
  count=mod(count+1,5);
end
PhiP=newPhiP;

%count = 1;
%for i=0:(size(PhiP,1)/actions)-1;
	%for j=0:(actions-1)
		%newPhiP(i+count,[1+(j*features):(j*features)+features]) = PhiP(i+count,:);
        %count = count +1;
    %end
    %count = count - 1;
%end 

%PhiP = newPhiP;
