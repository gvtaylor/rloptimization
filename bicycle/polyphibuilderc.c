#include "mex.h"
#include "bilib.h"
#include "string.h"
/*#include "mexcurrent.h"
#include "mexexpected_next.h"*/

#define DEBUG 1

const int width=159;

void fillPhis(double** phi, double* states, double* states_dim) 
{
  double state[11];
  double nextstate[11];
  double row[width];
  double bigrow[width];
  double reward, totalreward;
  int i,j,k;
  int a;
  /*printf("inside fillPhis\n");*/
  for (i=0; i<states_dim[0]; i++)
  {
    for (j=0; j<11; j++)
      state[j]=states[j*(int)states_dim[0]+i];
    calcPolys(state, row);
    for (j=0; j<width; j++)
      phi[0][j*(int)states_dim[0]+i]=row[j];
  }
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
  double *states, *state, *therow, *phi[1];
  double numSamp;
  double states_dim[2];
  const mwSize *states_dim_array;
  int i,j;
  if (nrhs != 1)
  {
    mexErrMsgTxt("Incorrect number of input arguments; must have 2.");
  }

  /*  Create a pointer to the input matrix state */
  states = (double *)mxGetPr(prhs[0]);
  states_dim_array = mxGetDimensions(prhs[0]);
  if (states_dim_array[1] != 11)
  {
    printf("size %d %d\n",states_dim_array[0], states_dim_array[1]);
    mexErrMsgTxt("states is of bad dimension");
  }
  
  /*printf("width is %d %g\n",width,width);*/

  plhs[0]=mxCreateDoubleMatrix(states_dim_array[0],width,mxREAL);
  phi[0]=mxGetPr(plhs[0]);
  /*printf("before allocation\n");*/
  states_dim[0]=states_dim_array[0];
  states_dim[1]=states_dim_array[1];
  /*printf("about to call\n");*/
  fillPhis(phi, states, states_dim);
  /*printf("back from calling\n");*/
}
