function [runData success w]=tomScript(numTrajs, psi)
%%%%%
%
% numTrajs is the number of sample trajectories to draw from the domain.  A
% trajectory is, starting from the start spot, perform random actions until
% failure, or 35 steps is achieved.
%
% psi is the L1 regularization parameter
%
%%%%%

samps=bicycle_samples(numTrajs); %Draw the samples - really only use s
samps
theData = l1_alp_data('bicycle',.9,[],[],samps); %Build the object
theData.buildfeatures();  %Create features for states and next states for all
                          %possible actions
theData.setRho();         %Just uniform - in there for future development
disp('calc weights')
tic;
theData.buildweights(psi); %Calculate weights
toc
disp('done')
[runData success]=testIt(theData); %Test resulting policy
w=theData.weights;
