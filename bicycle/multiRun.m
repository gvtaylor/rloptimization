function [manyUnis manyNons]=multiRun(psis, numSamps, numRuns)

manyUnis=zeros(size(psis,2),size(numSamps,2));
manyNons=zeros(size(psis,2),size(numSamps,2));
for p=1:size(psis,2)
  unis=size(numSamps,2);
  nons=size(numSamps,2);
  for s=1:size(numSamps,2)
    disp(['Psi: ' num2str(psis(p)) ' Samps: ' num2str(numSamps(s))]);
    successcount=zeros(numRuns,1);
    parfor i=1:numRuns
      samps=uniformSamp(numSamps(s));
      g=l1_alp_data('bicycle',.9,[],[],samps);
      g.buildfeatures();
      g.setRho(1);
      g.buildweights(psis(p));
      %successcount(i)=testIt(g);
      [successcount(i) exper]=testIt2(g);
    end
    manyUnis(p,s)=sum(successcount)/numRuns;
    successcount=zeros(numRuns,1);
    parfor i=1:numRuns
      samps=nonuniformSamp(numSamps(s));
      g=l1_alp_data('bicycle',.9,[],[],samps);
      g.buildfeatures();
      g.setRho(1);
      g.buildweights(psis(p));
      %successcount(i)=testIt(g);
      [successcount(i) exper]=testIt2(g);
    end
    manyNons(p,s)=sum(successcount)/numRuns;
  end
  figure;
  plot(numSamps,manyUnis(p,:),numSamps,manyNons(p,:));
  title(['psi=' num2str(psis(p))]);
  manyUnis(p,:)=unis;
  manyNons(p,:)=nons;
end
