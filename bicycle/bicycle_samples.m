function [samp] = bicycle_samples(numSamples)
  n = numSamples;
  states = zeros(97*n,11);
  actions = zeros(97*n,1);
  rewards = zeros(97*n,1);
  nextstates = zeros(97*n,11);

  i = 0;
  thiscount=0;
  episodes=1;
  s = bicycle_initialize_state(@bicycle_simulator);
  while episodes <= n
    i = i + 1;
    a = ceil(rand*5);
    [ns,r,endsim] = bicycle_simulator(s,a);
    states(i,:) = s;
    actions(i) = a;
    rewards(i) = r;
    nextstates(i,:) = ns;
    thiscount=thiscount+1;
    if endsim || thiscount>=35
      s = bicycle_initialize_state(@bicycle_simulator);
      episodes=episodes+1;
      thiscount = 0;
    else
      s = ns;
    end
  end
  states=states(1:i,:);
  actions=actions(1:i,:);
  rewards=rewards(1:i,:);
  nextstates=nextstates(1:i,:);
  samp=samples(states,actions,rewards,nextstates);
end
