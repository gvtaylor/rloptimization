% Load and solve a linear program using adaptive PDHG.  The problem 
% solved is of the form
%            minimixe     <f,x>
%            subject to
%                     Ain*x<=bin     (inequalities)
%                     Aeq*x=beq      (equalities)
%                     lb <= x <= ub  (lower/upper bound)

%  This File requires that 'pdhg_adaptive.m' be in your current path. 
%
%  For an explanation of PDHG and how it applies to this problem, see
%    "Adaptive Primal-Dual Hybrid Gradient Methods for Saddle-Point
%    Problems"  available at <http://arxiv.org/abs/1305.0546>
%


%% Seedn random number generator
%randn('seed',0);
%rand('seed',0);

%% Load the problem sc50b
% load sc50b;
%  Note: 'f' was defined by sc50b, so we don't define it here
%  Ain=A;
%  bin=b;
%  Aeq=Aeq;
%  beq=beq;
%  %ub = 1e100;
%  %lb = 0;
% ub = Inf*ones(size(Ain,2),1);       %  Solution must be less than 100
% lb = -Inf*ones(size(Ain,2),1);         %  Solution must be greater than 100

%%  Create random problem
N = 200;  % number of variables
M = 50;   % number of constraints
f = rand(N,1);  % The objective
x = rand(N,1);  % create random positive vector
Ain = randn(M,N); % Create constraint matrix
bin = Ain*x;    % create constraint right hand side.  Note:  This construction guarantees that the problem is feasible
Aeq = [];%randn(1,N);       % There are no equality constraints
beq = [];%Aeq*x;
ub = 100*ones(N,1);       %  Solution must be less than 100
lb = 0*ones(N,1);         %  Solution must be greater than 100


%% Record exact solution
fprintf('\tComputing Exact Solution to LP\n');
tic;
exact = linprog(f,Ain,bin,Aeq,beq,lb,ub);
time = toc;
fprintf('\t\tsolution took %f seconds\n', time);

opts = [];
opts.tol = 1e-6;
[ sol, outs ] = pdhg_lp( f,Ain,bin,Aeq,beq, ub, lb, opts );
semilogy(outs.p.^2+outs.d.^2)
fprintf('Relative Error = %f\n',norm(sol-exact)/norm(exact));
