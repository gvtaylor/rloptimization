function [times]=timeTrial(numTrajs, numRuns, psi)

times=zeros(2,numRuns);
for i=1:numRuns
  samps=bicycle_samples(numTrajs);
  theData=l1_alp_data('bicycle',.9,[],[],samps);
  theData.buildfeatures();
  theData.setRho()
  theData.t=psi;
  [w1 result time1]=ralp(theData);
  [w2 time2]=callAdaptive(theData);
  times(:,i)=[time1;time2];
end
