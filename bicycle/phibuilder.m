function  [features,alpha,wfactors]=phibuilder(data)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Constructs features and expected next features for a set of samples
% using all four actions
%
% Inputs -  data:         l1_alp_data object
%
% Outputs - features:     Cell matrix of feature and next 
%                           feature matrices
%           alpha and wfactors: Feature standardization parameters
%
% Uses polyphibuilderc to calculate features
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

s=data.samples.s;
Phi=polyphibuilderc(s);
ns1=zeros(size(s));
ns2=zeros(size(s));
ns3=zeros(size(s));
ns4=zeros(size(s));
ns5=zeros(size(s));
r1=zeros(size(s,1),1);
r2=zeros(size(s,1),1);
r3=zeros(size(s,1),1);
r4=zeros(size(s,1),1);
r5=zeros(size(s,1),1);
for i=1:size(data.samples.s,1)
  [ns1(i,:),r(i),es]=bicycle_simulator(s(i,:),1);
  [ns2(i,:),r(i),es]=bicycle_simulator(s(i,:),2);
  [ns3(i,:),r(i),es]=bicycle_simulator(s(i,:),3);
  [ns4(i,:),r(i),es]=bicycle_simulator(s(i,:),4);
  [ns5(i,:),r(i),es]=bicycle_simulator(s(i,:),5);
end
Phip1=polyphibuilderc(ns1);
Phip2=polyphibuilderc(ns2);
Phip3=polyphibuilderc(ns3);
Phip4=polyphibuilderc(ns4);
Phip5=polyphibuilderc(ns5);
[Phi,wfactors,alpha]=standardize(Phi);
[n k]=size(Phi);
Phi=[ones(n,1) Phi];
features{1}=repmat(Phi,5,1);
clear Phi;
Phip1=(Phip1-repmat(alpha,size(Phip1,1),1))./repmat(wfactors,size(Phip1,1),1);
Phip1=[ones(size(Phip1,1),1) Phip1];
Phip2=(Phip2-repmat(alpha,size(Phip2,1),1))./repmat(wfactors,size(Phip2,1),1);
Phip2=[ones(size(Phip2,1),1) Phip2];
Phip3=(Phip3-repmat(alpha,size(Phip3,1),1))./repmat(wfactors,size(Phip3,1),1);
Phip3=[ones(size(Phip3,1),1) Phip3];
Phip4=(Phip4-repmat(alpha,size(Phip4,1),1))./repmat(wfactors,size(Phip4,1),1);
Phip4=[ones(size(Phip4,1),1) Phip4];
Phip5=(Phip5-repmat(alpha,size(Phip5,1),1))./repmat(wfactors,size(Phip5,1),1);
Phip5=[ones(size(Phip5,1),1) Phip5];
features{2}=[Phip1; Phip2; Phip3; Phip4; Phip5];
data.samples.r=[r1;r2;r3;r4;r5];
clear Phip;
return;
