#include <math.h>
#include <stdio.h>
#include "bilib.h"
#include "bisimulator.h"
#include "mex.h"

double poly_kernel( double d,
                    double* state1, double* state2)
{
/*  printf("inside poly_kernel %g\n",d);*/
  double product=1;
  int i;
  for (i=0; i<6; i++)
  {
    product=product*pow(state1[i]*state2[i]+1,d);
  }
  return product;
}

double gaussian_kernel(double sigma,
                       double* state1, double* state2)
{
  double product=1;
  int i;
  for (i=0; i<6; i++)
  {
    product=product*exp(-1*(pow(state2[i]-state1[i],2))/(2*pow(sigma,2)));
  }
  return product;
}

void current( double* row,
              double* state, double param, double* s, int srows,
              double (*kernel)(double,double*,double*))
{
  int i,j;
  double state2[6];
  for (i=0; i < srows; i++)
  {
    state2[0]=s[i]/1.3963;
    state2[1]=s[srows+i]/9;
    state2[2]=s[2*srows+i]/.2094;
    state2[3]=s[3*srows+i]/1.949;
    state2[4]=s[4*srows+i]/8.5249;
    state2[5]=s[5*srows+i]/M_PI;
    row[i] = (*kernel)( param, state, state2);
  }
}

double next_state(double* state, double a, double* nextstate)
{
  double theaction[2];
  double reward, maxnoise;
  double* endsim;
  maxnoise=.02;
  if (a==1)
  {
    theaction[0]=0;
    theaction[1]=-.02;
  } else if (a==2)
  {
    theaction[0]=0;
    theaction[1]=0;
  } else if (a==3)
  {
    theaction[0]=0;
    theaction[1]=.02;
  } else if (a==4)
  {
    theaction[0]=2;
    theaction[1]=0;
  } else if (a==5)
  {
    theaction[0]=-2;
    theaction[1]=0;
  } else {
    mexErrMsgTxt("Bad action!");
  }
  bicycle(nextstate, &reward, &endsim, state, theaction, 2, &maxnoise);
  return reward;
}

void normstate(double* state, double* normed)
{
  normed[0]=state[0]*10/1.3963;
  normed[1]=state[1]*10/9.2930;
  normed[2]=state[2]*10/.2094;
  normed[3]=state[3]*10/1.7527;
  normed[4]=state[4]*10/8.4872;
  normed[5]=state[5]*10/3.1319;
}
  
void calcPolys(double* state, double* row)
{
  int i, j, k, l, count;
  double normed[6];
  normstate(state,normed);
  count=0;
  for (i=0; i<6; i++)
    for (j=1; j<5; j++)
      row[count++]=pow(normed[i],j);

  for (i=0; i<5; i++)
    for (j=i+1; j<6; j++)
      for (k=1; k<4; k++)
        for (l=1; l<4; l++)
          row[count++]=pow(normed[i],k)*pow(normed[j],l);
  /*row[count++]=exp(-1*(pow(normed[2],2))/(2*pow(2,2)));
  row[count++]=exp(-1*(pow(normed[3],2))/(2*pow(2,2)));*/
  /*printf("just added feature %g from %g\n",row[count],normed[2]);*/
}
