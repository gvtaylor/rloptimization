function times=sizeFind(numTrajArr, numRuns, psi)
%times(:,1) is for gurobi, times(:,2) is for pdhg

numTrajCount=prod(size(numTrajArr));
times=zeros(numTrajCount,2);
for i=1:numTrajCount
  subTimes=timeTrial(numTrajArr(i),numRuns,psi);
  times(i,:)=mean(subTimes');
end
