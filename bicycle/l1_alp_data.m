classdef l1_alp_data < handle

%%%%%%%%%%%%%%%%%%
% Gavin Taylor (gvtaylor@cs.duke.edu)
%
% MATLAB class to handle l1-regularized ALP problems
%
% Usage:
%   g = l1_alp_data(problem, gamma, k1params, k2params, samples);
%   g.buildfeatures(numsamples);
%   g.buildweights(t);
%   g.value;
%%%%%%%%%%%%%%%%%%

  properties
    problem   % string of which directory problem-specific code
              %   can be found
    gamma     % discount factor
    samples   % structure defined by "samples.m"
    k1params  % vector of parameters for first kernel type
    k2params  % vector of parameters for second kernel type
    features  % cell array of features and expected next features
    alpha     % for feature normalizing
    wfactors  % for feature normalizing
    t         % sum(weights(1:end-1))<=t
    weights   % weights for features
    subsamples% indices of samples chosen for feature construction
    rho
    actChunks
    filename
    kSig
  end

  methods
    function obj = l1_alp_data(problem, gamma, k1params, k2params, s, a, r, sp)
    % if 8 arguments, will construct "samples" from the last four
    % if 5 arguments, will expect last argument to be of class "samples"
    % if no arguments,, will create empty object
      if nargin == 8
        obj.problem=problem;
        obj.samples=samples(s,a,r,sp);
        obj.gamma=gamma;
        obj.k1params=k1params;
        obj.k2params=k2params;
      elseif nargin == 5
        obj.problem=problem;
        obj.samples=s;
        obj.gamma=gamma;
        obj.k1params=k1params;
        obj.k2params=k2params;
      elseif nargin ~=0
        error('Bizarre number of inputs');
      end
    end

    function setscales(self, scales)
      self.scales=scales;
    end

    function setRho(self)
      self.rho=buildRho(self,1);
    end
    
    function setKSig(self, kSig)
      self.kSig=kSig;
    end

    function setactChunks(self, actChunks)
      self.actChunks=actChunks;
    end

    function setproblem(self, problem)
    % Set problem field to input
      self.problem=problem;
    end

    function setsamples(self, s, a, r, sp)
    % Set samples field
    % If four inputs, will construct samples structure
    % If one, will assume it is a samples structure
      if nargin==5
        self.samples=samples(s,a,r,sp);
      else
        self.samples=s;
      end
    end

    function setsubsamples(self)
    % pick subsamples for features
      if isempty(self.samples.s)
        error('Cannot pick subsamples without samples!');
      end
      num=min(size(self.samples.s,1),5000);
      num=size(self.samples.s);
      permu=randperm(size(self.samples.s,1));
      self.subsamples=permu(1:num);
    end

    function setkparams(self,k1params,k2params)
    % Set k*params fields
      self.k1params=k1params;
      self.k2params=k2params;
    end

    function setgamma(self, gamma)
    % Change discount factor
      self.gamma=gamma;
    end

    function setfeatures(self,varargin)
    % Set features cell array to input features
      if size(varargin,2)==1 && iscell(varargin)
        sz=size(varargin{1},2);
        varargin=varargin{1};
      else
        sz=size(varargin,2);
      end
      for i=1:size(varargin,2)
        if iscell(varargin)
          self.features{i}=varargin{i};
        else
          self.features{i}=varargin(i);
        end
      end
    end

    function buildfeatures(self)
    % Build features using the problems "phibuilder.m"
      if isempty(self.samples.s)
        error('Cannot build features without samples!');
      end
      %if nargin<2
        %numsamples=100;
      %end
%      pth=which('phibuilder');
%      disp(['calling ' pth ' ...']); % Lets you double-check you're running
                                     %   the right one
%      clear pth;
      [refeats,alpha,wfactors]=phibuilder(self);
      %disp('  ...done.  Setting features');
      self.setfeatures(refeats);
      self.alpha=alpha;
      self.wfactors=wfactors+(wfactors==0);
    end

    function [w fmin solstat details]=buildweights(self,t)
    % Calls l1alp.m which constructs and runs the LP to calculate weights
    % Input argument is the desired sum of the weights
      if isempty(self.features)
        error('Cannot calculate weights without features!');
      end
      if isempty(self.samples.r)
        error('Cannot calculate weights without r!');
      end
      if isempty(self.gamma)
        error('Cannot calculate weights without gamma!');
      end
      %[w,fmin,solstat,details]=l1alp(self,t);
      self.t=t;
      [w result time]=ralp(self);
%      if ~strcmp(details.statstring,'optimal')
%        disp(details);
%        warning(['solstat not zero but is ',num2str(solstat)]);
      %end
      self.weights=w;
    end

    function V=value(self)
    % Runs the problem's "valuer.m" and returns the value for sampled points
    % in the space
      if isempty(self.weights)
        error('Cannot calculate V without weights!');
      end
      pth=which('valuer');
      disp(['calling ' pth ' ...']);
      clear pth;
      V=valuer(self);
    end
  end
end
