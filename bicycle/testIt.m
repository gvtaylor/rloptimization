function [runData success]=testIt(data)

maxIter=100000;

initial=bicycle_simulator;
states=zeros(maxIter,11);
nextstates=zeros(maxIter,11);
as=zeros(maxIter,1);
rs=zeros(maxIter,1);
states(1,:)=[zeros(1,6) initial(7:11)];
state(6)=pi/2;
for j=1:maxIter
  if j>1
    states(j,:)=nextstates(j-1,:);
  end
  ns=zeros(5,11);
  r=zeros(5,1);
  es=zeros(5,1);
  for a=1:5
    [ns(a,:), r(a), es(a)]=bicycle_simulator(states(j,:),a);
  end
  Phi=polyphibuilderc(ns);
  Phi=(Phi-repmat(data.alpha,size(Phi,1),1))./repmat(data.wfactors,size(Phi,1),1);
  Phi=[ones(5,1) Phi];
  V=r+data.gamma*Phi*data.weights;
  [y i]=max(V);
  nextstates(j,:)=ns(i,:);
  rs(j)=r(i);
  if es(i)==1
    success=0
    runData=samples(states(1:j,:),as(1:j),rs(1:j),nextstates(1:j,:));
    return
  elseif states(j,7)<20
    success=1
    runData=samples(states(1:j,:),as(1:j),rs(1:j),nextstates(1:j,:));
    return
  end
end
success=0;
runData=samples(states,as,rs,nextstates);
